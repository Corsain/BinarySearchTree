/*
Sam Adelson
3/5/15

Binary Search Tree Source File



#include "BinaryTree.h"

template <class T>
BTree<T>::BTree()
{
	root = NULL;
}

template <class T>
void BTree<T>::Add(T value)
{
	if (root == NULL)
	{
		root = new Node(value);
	}

	else
	{
		if (value < root->key)
		{
			Add(value, root->left);
		}

		else if (value >= root->key)
		{
			Add(value, root->right);
		}
	}
}

template <class T>
void BTree<T>::Add(T value, Node<T>* currNode)
{
	if (currNode == NULL)
	{
		currNode = new Node(value);
	}

	else
	{
		if (value < root->key)
		{
			Add(value, root->left);
		}

		else if (value >= root->key)
		{
			Add(value, root->right);
		}
	}
}

template <class T>
bool BTree<T>::Search(T value)
{
	Node* currNode = root;
	bool found = false;

	while ((found == false) && (currNode != NULL))
	{
		if (value == currNode->key)
		{
			found = true;
		}

		else if (value < currNode->key)
		{
			currNode = currNode->left;
		}

		else
		{
			currNode = currNode->right;
		}
	}

	return found;
}

template <class T>
T BTree<T>::Max()
{
	Node* currNode = root;

	while (currNode->right != NULL)
	{
		currNode = currNode->right;
	}

	return currNode->key;
}

template <class T>
T BTree<T>::Min()
{
	Node* currNode = root;

	while (currNode->left != NULL)
	{
		currNode = currNode->left;
	}

	return currNode->key;
}

*/