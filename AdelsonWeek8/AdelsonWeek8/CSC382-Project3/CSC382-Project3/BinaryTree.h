/*
Sam Adelson
3/5/15

Binary Search Tree Header File

*/

//#include "Node.h"

#include<fstream>
#include <cstring>
#include<string>
#include <iostream>
#include<stdio.h>
#include<time.h>

using namespace std;

template <class T>
class BTree
{
	struct Node
	{
		T key;
		Node * left;
		Node * right;

		Node(T data)
		{
			key = data;
			left = NULL;
			right = NULL;
		}
	};

public:
	Node* root;

	BTree();

	int ImportVals(string fileName);

	void Add(T value); //Add a node with the value to tree
	void Add(T value, Node * &currNode); //Recursive part of Add function

	void Delete(T value); //Delete a node from tree

	bool Search(T value); //Search the nodes for target value/key

	T Max(); //search nodes for largest key
	T Min(); //search nodes for smallest key
};

template <class T>
BTree<T>::BTree()
{
	root = NULL;
}

template <class T>
int BTree<T>::ImportVals(string fileName)
{
	//clock_t t;

	ifstream importFile;
	int temp = 0;

	

	importFile.open(fileName);

	//t = clock();

	while (importFile >> temp)
	{
		//cout << "boop" << endl;
		Add(temp);
	}

	importFile.close();

	//printf("It took me %d clicks (%f seconds).\n", t, ((float)t) / CLOCKS_PER_SEC);

	return 0;
}

template <class T>
void BTree<T>::Add(T value)
{
/*	if (root == NULL)
	{
		root = new Node(value);
	}

	else if (value < (root->key))
	{
		Add(value, root->left);
	}

	else
	{
		Add(value, root->right);
	}
	
*/
	Add(value, root);
}

/**/
template <class T>
void BTree<T>::Add(T value, Node* &currNode)
{
	if (currNode == NULL)
	{
		currNode = new Node(value);
		//cout << "boop" << endl;
		return;
	}

	else
	{
		if (value < currNode->key)
		{
			Add(value, currNode->left);
		}

		else if (value >= currNode->key)
		{
			Add(value, currNode->right);
		}
	}
}


template <class T>
bool BTree<T>::Search(T value)
{
	clock_t t;

	Node* currNode = root;
	bool found = false;

	while ((found == false) && (currNode != NULL))
	{
		if (value == currNode->key)
		{
			found = true;
		}

		else if (value < currNode->key)
		{
			currNode = currNode->left;
		}

		else
		{
			currNode = currNode->right;
		}
	}

	t = clock();

	printf("It took me %d clicks (%f seconds).\n", t, ((float)t) / CLOCKS_PER_SEC);

	return found;
}

template <class T>
T BTree<T>::Max()
{
	//clock_t t;

	if (root == NULL)
	{
		return 0;
	}

	Node* currNode = root;

	while (currNode->right != NULL)
	{
		currNode = currNode->right;
	}

	//t = clock();

	//printf("It took me %d clicks (%f seconds).\n", t, ((float)t) / CLOCKS_PER_SEC);

	return currNode->key;
}

template <class T>
T BTree<T>::Min()
{
	//clock_t t;

	if (root == NULL)
	{
		return 0;
	}

	Node* currNode = root;

	while (currNode->left != NULL)
	{
		currNode = currNode->left;
	}

	//t = clock();

	//printf("It took me %d clicks (%f seconds).\n", t, ((float)t) / CLOCKS_PER_SEC);

	return currNode->key;
}
