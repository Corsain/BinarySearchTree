/*
Sam Adelson
3/5/15

Node Source File

*/

#include "Node.h"

template <class T>
Node<T>::Node(T value)
{
	key = value;
	left = NULL;
	right = NULL;
}