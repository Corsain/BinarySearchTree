/*
	Sam Adelson
	3/5/15

	Node Header File

*/

#include <cstring>
#include <iostream>

using namespace std;

template<class T>
class Node
{
public:
	Node(T value);

	T key;
	Node* left;
	Node* right;
};