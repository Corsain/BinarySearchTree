/*
Sam Adelson
3/5/15

Main Source File

*/

#include "BinaryTree.h"

int main()
{
	//make the test tree
	BTree<int>* testTree = new BTree<int>();

	cout << endl;
	cout << "Adding things to tree..." << endl;

	//Add stuff into tree
	/*testTree->Add(5);
	testTree->Add(9);
	testTree->Add(4);
	testTree->Add(2);
	testTree->Add(3);
	testTree->Add(10);
	testTree->Add(0);*/

	//testTree->ImportVals("num10.dat");
	testTree->ImportVals("num100.dat");
	//testTree->ImportVals("num1000.dat");
	//testTree->ImportVals("num10000.dat");

	//bool check = testTree->Search(5);
	bool check = testTree->Search(100000);

	cout << check << endl;

	//cout << "Max value: " << testTree->Max() << endl;
	//cout << "Min value: " << testTree->Min() << endl;
	//cout << testTree->root->key << endl;

	system("Pause");

	
}